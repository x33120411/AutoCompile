﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class GitRestoreProcess : ProcessInfoBase
    {
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.FileName = "cmd.exe";
            StartInfo.WorkingDirectory = ProcessPath.DynamicGameProjectPath;
            //StartInfo.Arguments = "/c git restore .";
        }

        public override string GetSuccessMessage()
        {
            return "Git Restore: OK";
        }

        public override string GetFailedMessage()
        {
            return "Git Restore: OK";
        }
    }
}
