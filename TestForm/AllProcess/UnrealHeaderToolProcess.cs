﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class UnrealHeaderToolProcess : ProcessInfoBase
    {
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.FileName = ProcessPath.UnrealHeaderToolPath + "\\UnrealBuildTool.exe";
            StartInfo.Arguments = ProcessPath.UnrealBuildToolArg;
        }
    }
}
