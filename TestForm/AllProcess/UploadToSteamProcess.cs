﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class UploadToSteamProcess : ProcessInfoBase
    {
        string LastArgument = "";
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.UseShellExecute = true;
            StartInfo.WorkingDirectory = ProcessPath.SteamUploadTestingPath;
            StartInfo.FileName = Argument;
            LastArgument = Argument;
        }

        public override string GetSuccessMessage()
        {
            return "上傳到Steam " + LastArgument + ": OK";
        }

        public override string GetFailedMessage()
        {
            return "上傳到Steam " + LastArgument + ": Failed";
        }
    }
}
