﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class GitPullProcess : ProcessInfoBase
    {
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.FileName = "cmd.exe";
            StartInfo.WorkingDirectory = ProcessPath.DynamicGameProjectPath;
            StartInfo.Arguments = "/c git pull " + "origin " + Argument;
        }

        public override string GetSuccessMessage()
        {
            DateTime DateTime = DateTime.Now;
            return "GitPull: OK" + " (執行時間" + DateTime.Hour + ":" + DateTime.Minute + ")";
        }

        public override string GetFailedMessage()
        {
            return "GitPull: Failed";
        }
    }
}
