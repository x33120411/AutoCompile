﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class GitCommitProcess : ProcessInfoBase
    {
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.FileName = "cmd.exe";
            StartInfo.WorkingDirectory = ProcessPath.DynamicGameProjectPath;
            StartInfo.Arguments = "/c git commit " + "-a -m " + "\"" +Argument + "\"";
        }

        public override string GetSuccessMessage()
        {
            return "GitCommit: OK";
        }

        public override string GetFailedMessage()
        {
            return "GitCommit: Failed";
        }
    }
}
