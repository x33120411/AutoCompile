﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class FileReplaceProcess : ProcessInfoBase
    {
        string FileReplaceConfiName = string.Empty;
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.UseShellExecute = false;
            StartInfo.WorkingDirectory = ProcessPath.DynamicGameProjectPath;
            StartInfo.FileName = "cmd.exe";
            FileReplaceConfiName = Argument;
            if (Argument == "Development")
            {
                StartInfo.Arguments = "/C powershell.exe /C " + ProcessPath.MasterFileReplacePath;
            }
            else
            {
                StartInfo.Arguments = "/C powershell.exe /C " + ProcessPath.ReleaseFileReplacePath;
            }
        }

        public override string GetSuccessMessage()
        {
            return "編譯設定切換" + FileReplaceConfiName + ": OK";
        }

        public override string GetFailedMessage()
        {
            return "編譯設定切換" + FileReplaceConfiName + ": Failed";
        }
    }
}
