﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class UATProcess : ProcessInfoBase
    {

        //public UATProcess(Action<object> caller) : base(caller)
        protected override void OnInitProcessToExecute(string Argument, string PlatformName)
        {
            base.OnInitProcessToExecute(Argument, PlatformName);
            StartInfo.FileName = ProcessPath.EngineBatchFolderPath + "\\RunUAT.bat";
            StartInfo.Arguments = Argument;
        }

        public override string GetSuccessMessage()
        {
            return "UAT " + ArgumentPlatformName + ": OK";
        }
        public override string GetFailedMessage()
        {
            return "UAT " + ArgumentPlatformName + ": Failed";
        }
    }
}
