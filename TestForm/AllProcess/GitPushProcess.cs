﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class GitPushProcess : ProcessInfoBase
    {
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.FileName = "cmd.exe";
            StartInfo.WorkingDirectory = ProcessPath.DynamicGameProjectPath;
            StartInfo.Arguments = "/c git push " + "origin " + Argument;
        }

        public override string GetSuccessMessage()
        {
            return "GitPush: OK";
        }

        public override string GetFailedMessage()
        {
            return "GitPush: Failed";
        }
    }
}
