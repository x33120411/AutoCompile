﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.LinkLabel;

namespace AutoCompile.AllProcess
{
    internal class ProcessPath
    {
        //Programe
        static public string DynamicGameProjectPath = "";
        static public string DynamicGameProjectPathSlash = "";
        static public string UnrealHeaderToolPath = "";
        static public string MSBuildPath = "";
        static public string GameProjectPathMaster = "";
        static public string GameProjectPathRelease = "";
        static public string RsyncFolder = "";
        static public string BuildServerPath = "";
        static public string WindowsFileModify = "";
        static public string PhythonPath = "";
        static public string DiscordBotPath = "";
        static public string EngineBatchFolderPath = "";
        static public string EngineBinWin64FolderPath = "";
        static public string SteamUploadTestingPath = "";
        static public string MasterFileReplacePath = "";
        static public string ReleaseFileReplacePath = "";

        //Argument
        static public string BuildWin64Arg = "";
        static public string BuildLinuxArg = "";
        static public string UATBuildWin64RocketArg = "";
        static public string UATBuildLinuxRocketArg = "";
        static public string UATBuildWin64ClientEditorBuild = "";
        static public string UATBuildWin64ClientArg = "";
        static public string UATBuildWin64ServerArg = "";
        static public string UATBuildLinuxServertArg = "";
        //static public string BuildMapArg = DynamicGameProjectPath + "/CF1.uproject" + " -run=resavepackages -BuildAll -Quality=Production -allowcommandletrendering -map=";
        static public string UnrealBuildToolArg = "";
        static public string BuildMapArg = "";
        
        static public void UpdateDynamicPath(string BranchName)
        {
            if (BranchName == "master")
            {
                DynamicGameProjectPath = GameProjectPathMaster;
            }
            else
            {
                DynamicGameProjectPath = GameProjectPathRelease;
            }
            UpdateOtherPath();
        }

        static private void UpdateOtherPath()
        {
            DynamicGameProjectPathSlash = DynamicGameProjectPath.Replace("\\", "/");
            UnrealHeaderToolPath = "D:\\UE5_CF_Fork\\Engine\\Binaries\\DotNET";
            MSBuildPath = "C:\\Program Files\\Microsoft Visual Studio\\2022\\Community\\MSBuild\\Current\\Bin\\MSBuild.exe";
            GameProjectPathMaster = "D:\\CODE\\CrossingFrontier";
            GameProjectPathRelease = "D:\\CODE\\CFR";
            RsyncFolder = "C:\\cwrsync_6.2.5_x64_free\\";
            BuildServerPath = "D:\\CODE\\CrossingFrontier\\build_server.ps1";
            WindowsFileModify = "D:\\CODE\\CrossingFrontier\\WindowsFileModify.ps1";
            PhythonPath = "C:\\Users\\User\\AppData\\Local\\Programs\\Python\\Python310\\pythonw.exe";
            DiscordBotPath = "D:\\AutoCompile\\CompileStatusDCBot\\main.py";
            EngineBatchFolderPath = "D:\\UE5_CF_Fork\\Engine\\Build\\BatchFiles";
            EngineBinWin64FolderPath = "D:\\UE5_CF_Fork\\Engine\\Binaries\\Win64";
            SteamUploadTestingPath = "C:\\sdk\\tools\\ContentBuilder";
            MasterFileReplacePath = "D:\\CODE\\CrossingFrontier\\Master_FileReplaceScript.ps1";
            ReleaseFileReplacePath = "D:\\CODE\\CrossingFrontier\\Release_FileReplaceScript.ps1";

            BuildWin64Arg = "CF1Server Win64 Development -Project=\"" + DynamicGameProjectPathSlash + "/CF1.uproject" + "\" -WaitMutex -FromMsBuild -skipcook -iterate";
            BuildLinuxArg = "CF1Server Linux Development -Project=\"" + DynamicGameProjectPathSlash + "/CF1.uproject" + "\" -WaitMutex -FromMsBuild -skipcook -iterate";
            UATBuildWin64RocketArg = "BuildCookRun -Project=" + DynamicGameProjectPathSlash + "/CF1.uproject" + " -NoP4 -NoCompileEditor -Distribution -TargetPlatform=Win64 -Platform=Win64 -ClientConfig=Development -ServerConfig=Development -Cook -Map=Aserima_DeathMatchEX1.umap+Aserima_Pratice.umap -Build -Stage -Pak -Archive -ArchiveDirectory=" + DynamicGameProjectPathSlash + " -Rocket -Prereqs -Package -nodebuginfo -utf8output -ddc=DerivedDataBackendGraph -iterate";
            UATBuildLinuxRocketArg = "BuildCookRun -Project=" + DynamicGameProjectPathSlash + "/CF1.uproject" + " -NoP4 -NoCompileEditor -Distribution -TargetPlatform=Linux -Platform=Win64 -ClientConfig=Development -ServerConfig=Development -Cook -Map=Aserima_DeathMatchEX1.umap+Aserima_Pratice.umap -Build -Stage -Pak -Archive -ArchiveDirectory=" + DynamicGameProjectPathSlash + " -Rocket -Prereqs -Package -nodebuginfo -utf8output -ddc=DerivedDataBackendGraph -iterate";

            UATBuildWin64ClientEditorBuild = "BuildTarget -project=" + DynamicGameProjectPathSlash + "/CF1.uproject -platform=Win64 -configuration=Test -target=Editor+Game";
            UATBuildWin64ClientArg = "-ScriptsForProject=" + DynamicGameProjectPathSlash + "/CF1.uproject" + " BuildCookRun -nocompileeditor -installed -nop4 -project=" + DynamicGameProjectPathSlash + "/CF1.uproject" + " -cook -stage -archive -archivedirectory=" + DynamicGameProjectPathSlash + " -package -compressed -SkipCookingEditorContent -ddc=InstalledDerivedDataBackendGraph -build -pak -prereqs -nodebuginfo -targetplatform=Win64 -build -target=CF1 -clientconfig=Test -utf8output -CrashReporter";
            UATBuildWin64ServerArg = "-ScriptsForProject=" + DynamicGameProjectPathSlash + "/CF1.uproject Turnkey -command=VerifySdk -platform=Win64 -UpdateIfNeeded -project=" + DynamicGameProjectPathSlash + "/CF1.uproject BuildCookRun -nop4 -utf8output -nocompileeditor -skipbuildeditor -stage -cook -project=" + DynamicGameProjectPathSlash + "/CF1.uproject - target=CF1Server -unrealexe=\"D:/UE5_CF_Fork/Engine/Binaries/Win64/UnrealEditor-Cmd.exe\" -platform=Win64 -SkipCookingEditorContent -archive -package -build -pak -compressed -prereqs -archivedirectory=" + DynamicGameProjectPathSlash + " -server -noclient -serverconfig=Development -nocompileuat"; 
            BuildMapArg = DynamicGameProjectPath + "/CF1.uproject" + " -skipcompile -CF1BuildMap -maps=";
            UATBuildLinuxServertArg = "-ScriptsForProject=" + DynamicGameProjectPathSlash + "/CF1.uproject Turnkey -command=VerifySdk -platform=Linux -UpdateIfNeeded -project=" + DynamicGameProjectPathSlash + "/CF1.uproject BuildCookRun -nop4 -utf8output -nocompileeditor -skipbuildeditor -stage -cook -project=" + DynamicGameProjectPathSlash + "/CF1.uproject -target=CF1Server -unrealexe=\"D:/UE5_CF_Fork/Engine/Binaries/Win64/UnrealEditor-Cmd.exe\" -platform=Linux -SkipCookingEditorContent -archive -package -build -pak -compressed -prereqs -archivedirectory=" + DynamicGameProjectPathSlash + " -server -noclient -serverconfig=Development -nocompile -nocompileuat -iterate -CrashReporter";
            UnrealBuildToolArg = "-projectfiles -project=" + DynamicGameProjectPathSlash + "/CF1.uproject" + " -game -rocket -progress -DIFFONLY -iterate";
        }
        //static public string FinalPackageArg = "BuildCookRun -rocket -compile -compileeditor -installed -nop4 -project=\"C:/CODE/CrossingFrontier/CF1.uproject\" -cook -stage -archive -archivedirectory=\"C:/CODE/CrossingFrontier/temp/Development/x64\" -package -clientconfig=Development -ue4exe=UE4Editor-Cmd.exe -clean -pak -prereqs -distribution -nodebuginfo -targetplatform=Win64 -build -utf8output";

        //private const string DiscordBotPath = "C:\\AutoCompile\\CompileStatusDCBot\\main.py";
        //private const string EnginePath = "C:\\UE4.27\\Engine\\Build\\BatchFiles";
        //private const string ProjectPath = "C:\\CODE\\CrossingFrontier";
        //private const string PhythonPath = "C:\\Users\\User\\AppData\\Local\\Programs\\Python\\Python310\\pythonw.exe";
        //private const string BuildPath = "C:\\UE4.27\\Engine\\Build\\BatchFiles\\Build.bat";
        //private const string UATPath = "C:\\UE4.27\\Engine\\Build\\BatchFiles\\RunUAT.bat";
        //private const string BuildServerPath = "C:\\CODE\\CrossingFrontier\\build_server.ps1";
        //private const string RSyncPath = "C:\\cwrsync_6.2.5_x64_free\\cwrsync.cmd";
        //private const string BuildWin64Arg = "CF1Server Win64 Development -Project=\"C:/CODE/CrossingFrontier/CF1.uproject\" -WaitMutex -FromMsBuild -skipcook -iterate";
        //private const string BuildLinuxArg = "CF1Server Linux Development -Project=\"C:/CODE/CrossingFrontier/CF1.uproject\" -WaitMutex -FromMsBuild -skipcook -iterate";
        //private const string UATBuildWin64Arg = "-ScriptsForProject=C:/CODE/CrossingFrontier/CF1.uproject BuildCookRun -nocompileeditor -nop4 -project=C:/CODE/CrossingFrontier/CF1.uproject -cook -stage -archive -archivedirectory=C:/CODE/CrossingFrontier -package -ue4exe=C:\\UE4.27\\Engine\\Binaries\\Win64\\UE4Editor-Cmd.exe -compressed -SkipCookingEditorContent -ddc=DerivedDataBackendGraph -pak -prereqs -nodebuginfo -targetplatform=Win64 -build -target=CF1 -clientconfig=Development -utf8output -compile -skipcook";
    }
}
