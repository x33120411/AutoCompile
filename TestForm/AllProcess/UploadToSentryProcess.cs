﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class UploadToSentryProcess : ProcessInfoBase
    {
        string LastArgument = "";
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.UseShellExecute = true;
            StartInfo.WorkingDirectory = ProcessPath.DynamicGameProjectPathSlash;
            StartInfo.FileName = Argument;
            LastArgument = Argument;
        }

        public override string GetSuccessMessage()
        {
            return "上傳到Sentry: OK";
        }

        public override string GetFailedMessage()
        {
            return "上傳到Sentry: Failed";
        }
    }
}
