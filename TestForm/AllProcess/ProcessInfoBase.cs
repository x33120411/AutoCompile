﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using TestForm;

namespace AutoCompile.AllProcess
{

    enum EProcessResult
    {
         Error,
         Warning,
         Finished,
         None
    }

    internal class ProcessInfoBase : ICloneable
    {
        private static int PROCESS_SUCCESS_CODE = 0;
        public static List<string> MessageLog = new List<string>();
        public System.Windows.Forms.Timer ThreadCheckTimer = new System.Windows.Forms.Timer();
        public bool CancelWhenProcessFailed = false;
        public bool IsParallelProcess = false;

        protected Process ProcessObj = new Process();
        protected ProcessStartInfo StartInfo = new ProcessStartInfo();
        protected string ArgumentPlatformName = "";

        private bool NeedTriggerExitedEvent = false;
        private bool hasTriggerProcessBefore = false;
        private string ProcessDisplayName = "";
        private DateTime ProcessStartTime = DateTime.Now;
        public Action<object, EProcessResult> ProcessExitedFuncPtr;
        protected EProcessResult LastProcessResult = EProcessResult.None;

        public ProcessInfoBase()
        {
            //ProcessObj.Exited += ProcessExit_UnSafe;
            ProcessObj.EnableRaisingEvents = true;
            ProcessObj.StartInfo = StartInfo;
            //ThreadCheckTimer.Interval = 1000;
            //ThreadCheckTimer.Tick += new EventHandler(CheckThreadTimer);
            //ThreadCheckTimer.Start();
            //ProcessExitedFuncPtr = TestForm.GlobalVariable.ProcessExitAction;
        }

        public bool IsProcessExited()
        {
            return NeedTriggerExitedEvent;
        }

        public bool hasProcessRunningBefore()
        {
            return hasTriggerProcessBefore;
        }

        public int GetProcessID()
        {
            return ProcessObj.Id;
        }

        protected virtual EProcessResult CheckProcessResult()
        {
            if (ProcessObj.ExitCode == PROCESS_SUCCESS_CODE)
            {
                return EProcessResult.Finished;
            }
            return EProcessResult.Error;
        }

        protected virtual bool CheckProcessTriggerExitEvent()
        {
            return true;
        }

        protected virtual void OnProcessExitBeenIgnore()
        {
            // 進程關閉被無視但沒有給出相應的處置
            Debug.Assert(false);
        }

        private void CheckThreadTimer(object? myObject, EventArgs myEventArgs)
        {
            if (NeedTriggerExitedEvent)
            {
                LastProcessResult = CheckProcessResult();
                OnProcessExited();
                if (ProcessExitedFuncPtr != null && CheckProcessTriggerExitEvent())
                {
                    ProcessExitedFuncPtr(this, LastProcessResult);
                }
                else
                {
                    OnProcessExitBeenIgnore();
                }
                NeedTriggerExitedEvent = false;
            }
        }

        private void ProcessExit_UnSafe(object? sender, EventArgs e)
        {
            NeedTriggerExitedEvent = true;
        }

        public virtual string GetSuccessMessage()
        {
            return ProcessObj.StartInfo.FileName + " " + ArgumentPlatformName + ": OK";
        }

        public virtual string GetFailedMessage()
        {
            return ProcessObj.StartInfo.FileName + " " + ArgumentPlatformName + ": Failed";
        }

        public virtual string GetWarningMessage()
        {
            return ProcessObj.StartInfo.FileName + " " + ArgumentPlatformName + ": Warning";
        }

        public void SetProcessDisplayName(string Name)
        {
            ProcessDisplayName = Name;
        }

        public virtual void ExecuteProcess(string Argument, string Patformname)
        {
            OnInitProcessToExecute(Argument, Patformname);
            ProcessObj.Start();
            ProcessStartTime = DateTime.Now;
        }

        protected virtual void OnInitProcessToExecute(string Argument, string Platformname)
        {
            //不要在這裡修改Process.Exited不然ProcessExit會失效
            NeedTriggerExitedEvent = false;
            hasTriggerProcessBefore = true;
            ArgumentPlatformName = Platformname;
        }

        protected virtual void OnProcessExited()
        {
            ProcessObj.WaitForExit();
            string FinalMessage = "";
            if (LastProcessResult == EProcessResult.Finished)
            {
                FinalMessage = GetSuccessMessage();
            }
            else if (LastProcessResult == EProcessResult.Warning)
            {
                FinalMessage = GetWarningMessage();
            }
            else if (LastProcessResult == EProcessResult.Error)
            {
                FinalMessage = GetFailedMessage();
            }
            TimeSpan ExecutionTime = DateTime.Now - ProcessStartTime;
            FinalMessage += (IsParallelProcess ? " (Parallel Process)" : "") + $" 執行時間: {ExecutionTime.Hours}H:{ExecutionTime.Minutes}M:{ExecutionTime.Seconds}S";
            MessageLog.Add(FinalMessage);
            Reset();
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        public void Reset()
        {
            NeedTriggerExitedEvent = false;
            ThreadCheckTimer.Interval = 1000;
            ThreadCheckTimer.Tick -= new EventHandler(CheckThreadTimer);
            ThreadCheckTimer.Stop();
        }

        public void Init()
        {
            ProcessObj.Exited += ProcessExit_UnSafe;
            ThreadCheckTimer.Interval = 1000;
            ThreadCheckTimer.Tick += new EventHandler(CheckThreadTimer);
            ThreadCheckTimer.Start();
            ProcessExitedFuncPtr = TestForm.GlobalVariable.ProcessExitAction;
        }
    }
}
