﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class GitSwitchBranchProcess : ProcessInfoBase
    {
        private string LastArgument = "";
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.FileName = "cmd.exe";
            StartInfo.WorkingDirectory = ProcessPath.DynamicGameProjectPath;
            StartInfo.Arguments = "/c git switch " + Argument;
            LastArgument = Argument;
        }

        public override string GetSuccessMessage()
        {
            return "Git Switch To " + LastArgument + ": OK";
        }

        public override string GetFailedMessage()
        {
            return "Git Switch To " + LastArgument + ": Failed";
        }
    }
}
