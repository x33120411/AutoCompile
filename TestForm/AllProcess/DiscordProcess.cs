﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class DiscordProcess:ProcessInfoBase
    {
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            string ProcessLog = "";
            foreach (string str in MessageLog)
            {
                ProcessLog += str + "\n";
            }
            StartInfo.FileName = ProcessPath.PhythonPath;
            StartInfo.Arguments = ProcessPath.DiscordBotPath + " " + ProcessLog + Argument;
        }
    }
}
