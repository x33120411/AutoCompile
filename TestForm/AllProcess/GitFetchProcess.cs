﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class GitFetchProcess : ProcessInfoBase
    {
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.FileName = "C:\\Windows\\System32\\conhost.exe";
            StartInfo.WorkingDirectory = ProcessPath.DynamicGameProjectPath;
            StartInfo.Arguments = "/c git fetch --all";
        }

        public override string GetSuccessMessage()
        {
            return "Git Fetch: OK";
        }

        public override string GetFailedMessage()
        {
            return "Git Fetch: Failed";
        }
    }
}
