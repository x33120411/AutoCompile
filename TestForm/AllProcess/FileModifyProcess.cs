﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class FileModifyProcess:ProcessInfoBase
    {
        private string LastArgument = "";
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.UseShellExecute = false;
            StartInfo.WorkingDirectory = ProcessPath.DynamicGameProjectPath;
            StartInfo.FileName = "cmd.exe";
            StartInfo.Arguments = "/C powershell.exe /C " + Argument;
            LastArgument = Argument;
        }

        public override string GetSuccessMessage()
        {
            return $"{LastArgument}: OK";
        }

        public override string GetFailedMessage()
        {
            return $"{LastArgument}: Failed";
        }
    }
}
