﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestForm;
using static System.Net.Mime.MediaTypeNames;

namespace AutoCompile.AllProcess
{
    internal class BuildLightProcess : ProcessInfoBase
    {
        System.Windows.Forms.Timer CheckErrorTimer = new System.Windows.Forms.Timer();
        bool IsIgnoreExitEvent = false;
        private string LastArgument = "";
        string LastFailedMapName = "";
        string LastFailedMapPath = "";
        int RerunTimes = 0;
        public async void CheckErrorFromUE()
        {
            if (!File.Exists(ProcessPath.DynamicGameProjectPath + "\\Saved\\Logs\\CF1.log"))
            {
                return;
            }
            string CheckText = "with an invalid payload to package";
            string Text = "";
            using (var fs = new FileStream(ProcessPath.DynamicGameProjectPath + "\\Saved\\Logs\\CF1.log", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var sr = new StreamReader(fs, Encoding.Default))
            {
                Text = sr.ReadToEnd();
            }
            int CheckTextIndex = Text.IndexOf(CheckText);
            if (CheckTextIndex != -1)
            {
                int LineEndIndex = Text.IndexOf("\n", CheckTextIndex);
                string LineText = Text.Substring(CheckTextIndex + CheckText.Length, LineEndIndex - (CheckTextIndex + CheckText.Length));
                int LineMapStartIndex = LineText.IndexOf("'");
                int LineMapEndIndex = LineText.LastIndexOf("'");
                string MapPath = LineText.Substring(LineMapStartIndex + 1, LineMapEndIndex - LineMapStartIndex - 1);
                int MapNameStartIndex = MapPath.LastIndexOf("/");
                string MapName = MapPath.Substring(MapNameStartIndex + 1, MapPath.Length - 1 - MapNameStartIndex);
                int BuiltDataStartIndex = MapName.IndexOf("_BuiltData");
                MapName = MapName.Remove(BuiltDataStartIndex, MapName.Length - BuiltDataStartIndex);
                BuiltDataStartIndex = MapPath.IndexOf("_BuiltData");
                MapPath = MapPath.Remove(BuiltDataStartIndex, MapPath.Length - BuiltDataStartIndex);
                MapPath = MapPath.Substring(5, MapPath.Length - 5);
                LastFailedMapName = MapName;
                LastFailedMapPath = ProcessPath.DynamicGameProjectPathSlash + "/Content/" + MapPath;
                IsIgnoreExitEvent = true;
                CheckErrorTimer.Stop();
                Console.WriteLine("Failed Save. Killed UE Process");
                Form1.MainInstance.KillAllSubProcess();
                Console.WriteLine("Start HandleSaveErrorMap");
                HandleSaveErrorMap();
            }
        }

        private void HandleCheckErrorTimer(object? myObject, EventArgs myEventArgs)
        {
            CheckErrorFromUE();
        }

        protected override bool CheckProcessTriggerExitEvent()
        {
            return true;
        }

        protected void HandleSaveErrorMap()
        {
            // 刪除出問題的檔案
            {
                string BuiltDataPath = LastFailedMapPath + "/_BuiltData.uasset";
                File.Delete(LastFailedMapPath);
                if (File.Exists(BuiltDataPath))
                {
                    File.Delete(BuiltDataPath);
                }
                Console.WriteLine($"Failed Save. Delete File: {LastFailedMapPath}");
            }
            // git revert被刪除的檔案
            Process RevertProcess = new Process();
            RevertProcess.StartInfo.FileName = "C:\\Windows\\System32\\conhost.exe";
            RevertProcess.StartInfo.WorkingDirectory = ProcessPath.DynamicGameProjectPath;
            RevertProcess.StartInfo.Arguments = "/c git restore " + ProcessPath.DynamicGameProjectPathSlash + "/" + LastFailedMapPath;
            Console.WriteLine($"Failed Save. Restore file: {LastFailedMapPath}");
            RevertProcess.Start();
            RevertProcess.WaitForExit();
            Console.WriteLine($"Failed Save. Restore Complete for: {LastFailedMapPath}");
            string[] AllArgMap = LastArgument.Split("+");
            string FinalArg = "";
            bool FoundedErrorMap = false;
            foreach (string str in AllArgMap)
            {
                if (FoundedErrorMap || str == LastFailedMapName)
                {
                    FoundedErrorMap = true;
                    FinalArg += "+";
                    FinalArg += str;
                }
            }
            // 在執行一次
            Console.WriteLine($"Failed Save. Rerun UE");
            ExecuteProcess(FinalArg, "");
        }
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.FileName = ProcessPath.EngineBinWin64FolderPath + "\\UnrealEditor.exe";
            StartInfo.Arguments = ProcessPath.BuildMapArg + Argument;
            Console.WriteLine("BuildLightArg: " + ProcessPath.BuildMapArg + Argument);
            LastArgument = Argument;
            CheckErrorTimer.Interval = 1000;
            CheckErrorTimer.Tick += HandleCheckErrorTimer;
            CheckErrorTimer.Start();
        }

        protected override EProcessResult CheckProcessResult()
        {
            if (ProcessObj.ExitCode == 0)
            {
                return EProcessResult.Finished;
            }
            else if (ProcessObj.ExitCode == 1)
            {
                return EProcessResult.Warning;
            }
            else
            {
                return EProcessResult.Error;
            }
        }

        public override string GetSuccessMessage()
        {
            return LastArgument + "光影" + ": OK";
        }

        public override string GetFailedMessage()
        {
            return LastArgument + "光影" + ": Failed";
        }

        public override string GetWarningMessage()
        {
            return LastArgument + "光影" + ": Warning";
        }
    }
}
