﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class MSBuildProcess : ProcessInfoBase
    {
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.FileName = ProcessPath.MSBuildPath;
            StartInfo.Arguments = ProcessPath.DynamicGameProjectPath + "\\CF1.sln" + " /t:build /p:Configuration=\"Development Editor\";Platform=Win64;verbodity=diagnostic";
        }
    }
}
