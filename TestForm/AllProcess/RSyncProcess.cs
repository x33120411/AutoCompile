﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class RSyncProcess:ProcessInfoBase
    {
        string LastArgument = "";
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.FileName = ProcessPath.RsyncFolder + Argument + ".cmd";
            LastArgument = Argument;
        }

        public override string GetSuccessMessage()
        {
            return "上傳到" + LastArgument + ": OK";
        }

        public override string GetFailedMessage()
        {
            return "上傳到" + LastArgument + ": Failed";
        }
    }
}
