﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCompile.AllProcess
{
    internal class BuildProcess : ProcessInfoBase
    {
        protected override void OnInitProcessToExecute(string Argument, string Platformname)
        {
            base.OnInitProcessToExecute(Argument, Platformname);
            StartInfo.UseShellExecute = true;
            StartInfo.WorkingDirectory = ProcessPath.EngineBatchFolderPath;
            StartInfo.FileName = "Build.bat";
            StartInfo.Arguments = Argument;
        }
    }
}
