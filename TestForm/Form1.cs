using System.Diagnostics;
using System.Management;
using System.Collections;
using System.Runtime.InteropServices;
using AutoCompile.AllProcess;

namespace TestForm
{

    enum EProcessType
    {
        UAT,
        Build,
        Discord,
        FileModify,
        RSync,
        GitPull,
        BuildLight,
        MSBuild,
        UnrealHeaderTool,
        UploadToSteamTesting,
        FinalPackage,
        GitSwitchBranch,
        GitRestore,
        GitFetch,
        GitCommit,
        GitPush,
        FileReplace,
        UploadToSentry
    }

    struct ProcessQueueInfo
    {
        public EProcessType ProcessType;
        public string Argument;
        public bool CancelWhenProcessUnSuccess;
    }

    struct GlobalVariable
    {
        public static Action<object, EProcessResult>? ProcessExitAction;
    }

    public partial class Form1 : Form
    {
        //Process Info
        Queue<ProcessQueueInfo[]> ProcessQueue = new Queue<ProcessQueueInfo[]>();
        Hashtable ProcessInfoTable = new Hashtable();

        //Other
        bool hasCompileError = false;
        bool CancelWhenProcessFailed = false;
        bool isRunningParallel = false;
        List<ProcessInfoBase> WaitingProcess = new List<ProcessInfoBase>();
        string DiscordMessage;
        string ProcessLog;
        string CurrentRunningArg;
        string CurrentCompilePlatform;
        EProcessType CurrentRunningType;
        TimeSpan? ShowNextProcessTime;
        DateTime? NextProcessTime;
        DateTime? StartTime;
        System.Windows.Forms.Timer CheckTimeToProcessTimer = new System.Windows.Forms.Timer();

        //Instance
        public static Form1 MainInstance;
        public Form1()
        {
            InitializeComponent();
            HourComboBox.SelectedIndex = 0;
            MinutesComboBox.SelectedIndex = 0;
            //Git Branch Option
            GitBranchComboBox.Items.Add("master");
            GitBranchComboBox.Items.Add("re_2023_12_18");
            GitBranchComboBox.SelectedIndex = 0;
            //Build Configuration Option
            BuildConfigurationComboBox.SelectedIndex = 0;
            //End Git Branch Option
            TestForm.GlobalVariable.ProcessExitAction = OnProcessExited;
            LightQuelity.SelectedIndex = 0;
            MainInstance = this;
            FormClosed += Form1_Closing;
            Load += Form1_Load;
            CheckTimeToProcessTimer.Interval = 1000;
            CheckTimeToProcessTimer.Tick += new EventHandler(CheckTimeToProcess);
            CheckTimeToProcessTimer.Start();
            button3.Enabled = false;
            ProcessPath.UpdateDynamicPath("master");
            UpdateMapList();
            //Init Process Function Pointer
            UATProcess UATProcessObj = new UATProcess();
            DiscordProcess DiscordProcessObj = new DiscordProcess();
            BuildProcess BuildProcessObj = new BuildProcess();
            FileModifyProcess fileModifyProcessObj = new FileModifyProcess();
            RSyncProcess RSyncProcessObj = new RSyncProcess();
            GitPullProcess GitPullProcessObj = new GitPullProcess();
            BuildLightProcess BuildLightProcessObj = new BuildLightProcess();
            MSBuildProcess MSBuildProcessObj = new MSBuildProcess();
            UnrealHeaderToolProcess UnrealHeaderToolProcess = new UnrealHeaderToolProcess();
            UploadToSteamProcess UploadToSteamProcessObj = new UploadToSteamProcess();
            FinalPackage FinalPackageProcessObj = new FinalPackage();
            GitSwitchBranchProcess GitSwitchBranchProcessObj = new GitSwitchBranchProcess();
            GitRestoreProcess GitRestoreProcessObj = new GitRestoreProcess();
            GitFetchProcess GitFetchProcessObj = new GitFetchProcess();
            GitCommitProcess GitCommitProcessObj = new GitCommitProcess();
            GitPushProcess GitPushProcessObj = new GitPushProcess();
            FileReplaceProcess FileReplaceProcessObj = new FileReplaceProcess();
            UploadToSentryProcess UploadToSentryProcessObj = new UploadToSentryProcess();
            ProcessInfoTable.Add(EProcessType.UAT, UATProcessObj);
            ProcessInfoTable.Add(EProcessType.Discord, DiscordProcessObj);
            ProcessInfoTable.Add(EProcessType.FileModify, fileModifyProcessObj);
            ProcessInfoTable.Add(EProcessType.Build, BuildProcessObj);
            ProcessInfoTable.Add(EProcessType.RSync, RSyncProcessObj);
            ProcessInfoTable.Add(EProcessType.GitPull, GitPullProcessObj);
            ProcessInfoTable.Add(EProcessType.BuildLight, BuildLightProcessObj);
            ProcessInfoTable.Add(EProcessType.MSBuild, MSBuildProcessObj);
            ProcessInfoTable.Add(EProcessType.UnrealHeaderTool, UnrealHeaderToolProcess);
            ProcessInfoTable.Add(EProcessType.UploadToSteamTesting, UploadToSteamProcessObj);
            ProcessInfoTable.Add(EProcessType.FinalPackage, FinalPackageProcessObj);
            ProcessInfoTable.Add(EProcessType.GitSwitchBranch, GitSwitchBranchProcessObj);
            ProcessInfoTable.Add(EProcessType.GitRestore, GitRestoreProcessObj);
            ProcessInfoTable.Add(EProcessType.GitFetch, GitFetchProcessObj);
            ProcessInfoTable.Add(EProcessType.GitCommit, GitCommitProcessObj);
            ProcessInfoTable.Add(EProcessType.GitPush, GitPushProcessObj);
            ProcessInfoTable.Add(EProcessType.FileReplace, FileReplaceProcessObj);
            ProcessInfoTable.Add(EProcessType.UploadToSentry, UploadToSentryProcessObj);
        }

        public void KillAllSubProcess()
        {
            foreach (object obj in WaitingProcess)
            {
                ProcessInfoBase CurrentInfo = (obj as ProcessInfoBase);
                if (CurrentInfo != null)
                {
                    CurrentInfo.ThreadCheckTimer.Stop();
                    if (CurrentInfo.hasProcessRunningBefore())
                    {
                        KillProcessAndChildrens(CurrentInfo.GetProcessID());
                    }
                }
            }
        }

        private void Form1_Closing(object sender, FormClosedEventArgs e)
        {
            KillAllSubProcess();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            AllocConsole();
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();

        private void UpdateMapList()
        {
            if (!Directory.Exists(ProcessPath.DynamicGameProjectPath))
            {
                LightMapBuildCheckList.Items.Add("不存在的專案路徑");
                LightMapBuildCheckList.Enabled = false;
                return;
            }
            //Init LightMap List
            LightMapBuildCheckList.Items.Clear();
            List<string> FinalMapPaths = new List<string>();
            string[] MapResourceMapNames = Directory.GetFiles(ProcessPath.DynamicGameProjectPath + "\\Content\\MapResource");
            string[] PVEMapNames = Directory.GetFiles(ProcessPath.DynamicGameProjectPath + "\\Content\\PVE\\Level", "*.*", SearchOption.AllDirectories);
            foreach (var str in MapResourceMapNames)
            {
                FinalMapPaths.Add(str);
            }
            foreach (var str in PVEMapNames)
            {
                FinalMapPaths.Add(str);
            }
            foreach (string MapPathName in FinalMapPaths)
            {
                int Index = MapPathName.LastIndexOf("\\");
                string MapName = MapPathName.Remove(0, Index + 1);
                if (MapName.EndsWith(".umap"))
                {
                    MapName = MapName.Remove(MapName.Length - 5, 5);
                    LightMapBuildCheckList.Items.Add(MapName);
                }
            }
        }

        private string GetBuildLightMapsAsArgument()
        {
            string OutArg = "";
            foreach (var Obj in LightMapBuildCheckList.CheckedItems)
            {
                if (OutArg.Length > 0)
                {
                    OutArg += "+";
                }
                string FinalStr = Obj.ToString().Replace(".umap", "");
                OutArg += FinalStr;
            }
            OutArg += " -Quality=" + LightQuelity.SelectedItem.ToString();
            return OutArg;
        }

        private void OnProcessExited(object caller, EProcessResult ProcessResult)
        {
            WaitingProcess.Remove(caller as ProcessInfoBase);
            if ((caller as ProcessInfoBase).CancelWhenProcessFailed && ProcessResult == EProcessResult.Error)
            {
                //處裡因進程失敗而中斷進程
                ProcessQueue.Clear();
                CancelWhenProcessFailed = false;
                ProcessLog += "中斷\n";
                label1.Text = "中斷\n" + ProcessLog;
                DiscordMessage = ProcessLog + "進程被中斷\\n";
                KillAllSubProcess();
                WaitingProcess.Clear();
                ResetUI();
                (ProcessInfoTable[EProcessType.Discord] as ProcessInfoBase).ExecuteProcess(DiscordMessage, "");
            }
            else if (WaitingProcess.Count() == 0)
            {
                RunNextProcess();
            }
            else if (!isRunningParallel)
            {
                // 基本上不可能跑到這裡 如果有肯定是WaitingProcess有問題
                Debug.Assert(false);
            }
        }

        private string GetCurrentCompilePlatform()
        {
            if (CurrentRunningArg == ProcessPath.BuildWin64Arg || CurrentRunningArg == ProcessPath.UATBuildWin64ClientArg ||
                CurrentRunningArg == ProcessPath.UATBuildWin64RocketArg)
            {
                return "Win64";
            }
            else if (CurrentRunningArg == ProcessPath.BuildLinuxArg || CurrentRunningArg == ProcessPath.UATBuildLinuxServertArg)
            {
                return "Linux";
            }
            return "";
        }

        private void RunNextProcess()
        {
            if (ProcessQueue.Count > 0)
            {
                //ProcessDequeue(out CurrentRunningType, out CurrentRunningArg, out CancelWhenProcessFailed);
                ProcessQueueInfo[] OutProcess = ProcessDequeue();
                Debug.Assert(OutProcess.Length > 0);
                isRunningParallel = OutProcess.Length > 1;
                foreach (ProcessQueueInfo Obj in OutProcess)
                {
                    string FinalArgument = Obj.Argument;
                    CurrentCompilePlatform = GetCurrentCompilePlatform();
                    if (Obj.ProcessType == EProcessType.Discord)
                    {
                        //Discord訊息例外處裡
                        DiscordMessage = "";
                        foreach (string str in ProcessInfoBase.MessageLog)
                        {
                            DiscordMessage += str + "\n";
                        }
                        DiscordMessage += hasCompileError ? "編譯失敗" : "編譯成功" + "\n";
                        TimeSpan? ExecutionTime = (DateTime.Now - StartTime);
                        DiscordMessage += $"總執行時間: {ExecutionTime.Value.Hours}H:{ExecutionTime.Value.Minutes}M:{ExecutionTime.Value.Seconds}S";
                        FinalArgument += DiscordMessage;
                    }
                    //Platform not completely
                    ProcessInfoBase CloneProcessObj = (ProcessInfoTable[Obj.ProcessType] as ProcessInfoBase).Clone() as ProcessInfoBase;
                    CloneProcessObj.IsParallelProcess = OutProcess.Length > 1;
                    CloneProcessObj.CancelWhenProcessFailed = Obj.CancelWhenProcessUnSuccess;
                    CloneProcessObj.Init();
                    WaitingProcess.Add(CloneProcessObj);
                    CloneProcessObj.ExecuteProcess(FinalArgument, GetCurrentCompilePlatform());
                }
            }
            else
            {
                OnProcessComplete();
            }
        }

        private void OnProcessComplete()
        {
            ResetUI();
            ProcessLog = "";
            foreach (string str in ProcessInfoBase.MessageLog)
            {
                ProcessLog += str + "\n";
            }
            ProcessInfoBase.MessageLog.Clear();
            label1.Text = "完成: \n" + ProcessLog;
            DiscordMessage = "";
            if (!CheckTimeToProcessTimer.Enabled)
            {
                CheckTimeToProcessTimer.Start();
            }
        }

        private void ResetUI()
        {
            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = false;
            checkedListBox1.Enabled = true;
            LightMapBuildCheckList.Enabled = true;
            label3.Text = "未排程";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PerformProcess();
        }

        private void PerformProcess()
        {
            StartTime = DateTime.Now;
            label1.Text = "編譯中...";
            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = true;
            checkedListBox1.Enabled = false;
            LightMapBuildCheckList.Enabled = false;
            ProcessLog = "";
            InitProcessQueue();
            RunNextProcess();
            CheckTimeToProcessTimer.Stop();
            if (NextProcessTime != null)
            {
                label3.Text = "排程已取消";
            }
            NextProcessTime = null;
        }

        private void InitProcessQueue()
        {
            if (checkedListBox3.GetItemChecked(0))
            {
                AddProcessToQueue(EProcessType.GitRestore, "");
            }
            AddProcessToQueue(EProcessType.GitFetch, "");
            AddProcessToQueue(EProcessType.GitSwitchBranch, GitBranchComboBox.Items[GitBranchComboBox.SelectedIndex].ToString(), true);
            if (checkedListBox3.GetItemChecked(1))
            {
                AddProcessToQueue(EProcessType.GitPull, GitBranchComboBox.Items[GitBranchComboBox.SelectedIndex].ToString(), true);
            }
            AddProcessToQueue(EProcessType.FileReplace, BuildConfigurationComboBox.Items[BuildConfigurationComboBox.SelectedIndex].ToString(), true);
            if (checkedListBox3.GetItemChecked(3))
            {
                if (LightMapBuildCheckList.CheckedItems.Count > 0)
                {
                    //只建光不編譯
                    AddProcessToQueue(EProcessType.BuildLight, GetBuildLightMapsAsArgument(), true);
                    AddProcessToQueue(EProcessType.Discord, "");
                }
                return;
            }
            else if (checkedListBox3.GetItemChecked(2))
            {
                if (LightMapBuildCheckList.CheckedItems.Count > 0)
                {
                    AddProcessToQueue(EProcessType.BuildLight, GetBuildLightMapsAsArgument(), true);
                }
            }
            AddProcessToQueue(EProcessType.UAT, ProcessPath.UATBuildWin64ClientEditorBuild, true);
            AddProcessToQueue(EProcessType.UAT, ProcessPath.UATBuildWin64ClientArg, true);
            AddProcessToQueue(EProcessType.UAT, ProcessPath.UATBuildWin64ServerArg, true);
            AddProcessToQueue(EProcessType.Build, ProcessPath.BuildWin64Arg, true);
            AddProcessToQueue(EProcessType.FileModify, ProcessPath.WindowsFileModify, true);
            AddProcessToQueue(EProcessType.UAT, ProcessPath.UATBuildLinuxServertArg, true);
            {
                // 到這裡Windows的部分全部完成了所以平行化LinuxServer與上傳Windows到Steam
                List<EProcessType> ProcessTypes = new List<EProcessType>();
                List<string> ProcessArgument = new List<string>();
                List<bool> CancelWhenProcessFailed = new List<bool>();
                //ProcessTypes.Add(EProcessType.Build);
                //ProcessArgument.Add(ProcessPath.BuildLinuxArg);
                //CancelWhenProcessFailed.Add(true);

                ProcessTypes.Add(EProcessType.UAT);
                ProcessArgument.Add(ProcessPath.UATBuildLinuxServertArg);
                CancelWhenProcessFailed.Add(true);
                if (checkedListBox2.GetItemChecked(0))
                {
                    ProcessTypes.Add(EProcessType.UploadToSteamTesting);
                    ProcessArgument.Add("run_build - testing.bat");
                    CancelWhenProcessFailed.Add(false);
                }
                if (checkedListBox2.GetItemChecked(1))
                {
                    ProcessTypes.Add(EProcessType.UploadToSteamTesting);
                    ProcessArgument.Add("run_build-tcp.bat");
                    CancelWhenProcessFailed.Add(false);

                    ProcessTypes.Add(EProcessType.UploadToSteamTesting);
                    ProcessArgument.Add("run_build.bat");
                    CancelWhenProcessFailed.Add(false);
                }
                if (checkedListBox2.GetItemChecked(2))
                {
                    ProcessTypes.Add(EProcessType.UploadToSteamTesting);
                    ProcessArgument.Add("run_build - testing2.bat");
                    CancelWhenProcessFailed.Add(false);
                }
                if (checkedListBox2.GetItemChecked(3))
                {
                    ProcessTypes.Add(EProcessType.UploadToSteamTesting);
                    ProcessArgument.Add("run_build - beta20230905.bat");
                    CancelWhenProcessFailed.Add(false);
                }
                // Steam上傳不能多開所以如果此次平行化有大於1個Steam上傳，那只有第一個會列入平行化其他的會在平行化結束後單獨執行
                List<EProcessType> SteamProcessType = new List<EProcessType>();
                List<string> SteamProcessArgument = new List<string>();
                List<bool> SteamCancelWhenProcessFailed = new List<bool>();
                int SteamProcessCount = 0;
                for (int i = ProcessTypes.Count - 1; i >= 0; i--)
                {
                    if (ProcessTypes[i] == EProcessType.UploadToSteamTesting)
                    {
                        SteamProcessCount++;
                        if (SteamProcessCount > 1)
                        {
                            SteamProcessType.Add(ProcessTypes[i]);
                            SteamProcessArgument.Add(ProcessArgument[i]);
                            SteamCancelWhenProcessFailed.Add(CancelWhenProcessFailed[i]);
                            ProcessTypes.RemoveAt(i);
                            ProcessArgument.RemoveAt(i);
                            CancelWhenProcessFailed.RemoveAt(i);
                        }
                    }
                }
                AddProcessToQueueParallel(ProcessTypes.ToArray(), ProcessArgument.ToArray(), CancelWhenProcessFailed.ToArray());

                if (SteamProcessType.Count > 0)
                {
                    for (int i = 0; i < SteamProcessType.Count; i++)
                    {
                        // 新增單獨執行的SteamUpload
                        AddProcessToQueue(SteamProcessType[i], SteamProcessArgument[i], SteamCancelWhenProcessFailed[i]);
                    }
                }
            }
            AddProcessToQueue(EProcessType.Build, ProcessPath.BuildLinuxArg, true);
            AddProcessToQueue(EProcessType.FileModify, ProcessPath.BuildServerPath, true);
            if (checkedListBox1.GetItemChecked(0))
            {
                AddProcessToQueue(EProcessType.RSync, "45.121.51.34");
            }
            if (checkedListBox1.GetItemChecked(1))
            {
                AddProcessToQueue(EProcessType.RSync, "114.32.129.153");
            }
            if (checkedListBox1.GetItemChecked(2))
            {
                AddProcessToQueue(EProcessType.RSync, "203.204.228.104");
            }
            if (checkedListBox1.GetItemChecked(3))
            {
                AddProcessToQueue(EProcessType.RSync, "203.204.228.101");
            }
            AddProcessToQueue(EProcessType.Discord, "");
            AddProcessToQueue(EProcessType.UploadToSentry, "UploadPdb.bat");
        }

        private void AddProcessToQueue(EProcessType processType, string argument, bool CancelWhenProcessUnSuccess = false)
        {
            ProcessQueueInfo Info;
            Info.ProcessType = processType;
            Info.Argument = argument;
            Info.CancelWhenProcessUnSuccess = CancelWhenProcessUnSuccess;
            ProcessQueueInfo[] ResultInfo = new ProcessQueueInfo[1];
            ResultInfo[0] = Info;
            ProcessQueue.Enqueue(ResultInfo);
        }

        private void AddProcessToQueueParallel(EProcessType[] processType, string[] argument, bool[] CancelWhenProcessUnSuccess)
        {
            List<ProcessQueueInfo> ProcessQueueInfo = new List<ProcessQueueInfo>();
            for (int i = 0; i < processType.Length; i++)
            {
                ProcessQueueInfo Info;
                Info.ProcessType = processType[i];
                Info.Argument = argument[i];
                Info.CancelWhenProcessUnSuccess = CancelWhenProcessUnSuccess[i];
                ProcessQueueInfo.Add(Info);
            }
            ProcessQueue.Enqueue(ProcessQueueInfo.ToArray());
        }

        private ProcessQueueInfo[] ProcessDequeue()
        {
            return ProcessQueue.Dequeue();
        }

        private static void KillProcessAndChildrens(int pid)
        {
            ManagementObjectSearcher processSearcher = new ManagementObjectSearcher
              ("Select * From Win32_Process Where ParentProcessID=" + pid);
            ManagementObjectCollection processCollection = processSearcher.Get();

            try
            {
                Process proc = Process.GetProcessById(pid);
                if (!proc.HasExited) proc.Kill();
            }
            catch (ArgumentException)
            {
                // Process already exited.
            }

            if (processCollection != null)
            {
                foreach (ManagementObject mo in processCollection)
                {
                    KillProcessAndChildrens(Convert.ToInt32(mo["ProcessID"])); //kill child processes(also kills childrens of childrens etc.)
                }
            }
        }

        private void UpdateProcessTimeText()
        {
            string FinalText = "";
            FinalText += HourComboBox.SelectedIndex < 10 ? "0" + HourComboBox.SelectedIndex.ToString() : HourComboBox.SelectedIndex.ToString();
            FinalText += ":";
            FinalText += MinutesComboBox.SelectedIndex < 10 ? "0" + MinutesComboBox.SelectedIndex.ToString() : MinutesComboBox.SelectedIndex.ToString();
            CountDownText.Text = "設定的時間: " + FinalText;
            label1.Text = "等待排程...";
        }

        private void UpdateProcessTimer()
        {
            DateTime PickTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, HourComboBox.SelectedIndex, MinutesComboBox.SelectedIndex, 0);
            if (PickTime < DateTime.Now)
            {
                PickTime = PickTime.AddDays(1);
            }

            NextProcessTime = PickTime;
        }

        private void HourComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateProcessTimeText();
        }

        private void MinutesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateProcessTimeText();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            UpdateProcessTimer();
        }

        private void CheckTimeToProcess(object? myObject, EventArgs myEventArgs)
        {
            if (NextProcessTime != null)
            {
                TimeSpan TimeDistance = NextProcessTime.Value - DateTime.Now;
                int FinalHours = 24 * TimeDistance.Days;
                FinalHours += TimeDistance.Hours;
                string TimeStr = "";
                TimeStr += FinalHours < 10 ? "0" + FinalHours.ToString() : FinalHours.ToString();
                TimeStr += ":" + (TimeDistance.Minutes < 10 ? "0" + TimeDistance.Minutes.ToString() : TimeDistance.Minutes.ToString());
                TimeStr += ":" + (TimeDistance.Seconds < 10 ? "0" + TimeDistance.Seconds.ToString() : TimeDistance.Seconds.ToString());
                label3.Text = "距離編譯開始還有: " + TimeStr + " (小時:分鐘:秒)";
                if (DateTime.Now.Year == NextProcessTime.Value.Year && DateTime.Now.Month == NextProcessTime.Value.Month &&
                    DateTime.Now.Day == NextProcessTime.Value.Day && DateTime.Now.Hour == NextProcessTime.Value.Hour &&
                    DateTime.Now.Minute == NextProcessTime.Value.Minute)
                {
                    PerformProcess();
                    label3.Text = "已執行排程";
                    NextProcessTime = null;
                }
            }
        }

        void CancelWholeProcess()
        {
            ProcessQueue.Clear();
            KillAllSubProcess();
            label1.Text = "取消成功";
            ResetUI();
            ProcessInfoBase.MessageLog.Clear();
            foreach (ProcessInfoBase Obj in WaitingProcess)
            {
                Obj.Reset();
            }
            WaitingProcess.Clear();
        }

        private void GitBranchComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProcessPath.UpdateDynamicPath(GitBranchComboBox.Items[GitBranchComboBox.SelectedIndex].ToString());
            if (ProcessPath.DynamicGameProjectPath != string.Empty)
            {
                UpdateMapList();
            }
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {

        }

        private void Form1_Load_2(object sender, EventArgs e)
        {

        }

        private void checkedListBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkedListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            CancelWholeProcess();
        }
    }
}