﻿namespace TestForm
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            button1 = new Button();
            label1 = new Label();
            HourComboBox = new ComboBox();
            label2 = new Label();
            MinutesComboBox = new ComboBox();
            CountDownText = new Label();
            button2 = new Button();
            label3 = new Label();
            checkedListBox1 = new CheckedListBox();
            label4 = new Label();
            label5 = new Label();
            LightMapBuildCheckList = new CheckedListBox();
            GitBranchComboBox = new ComboBox();
            label6 = new Label();
            checkedListBox2 = new CheckedListBox();
            checkedListBox3 = new CheckedListBox();
            button3 = new Button();
            label7 = new Label();
            LightQuelity = new ComboBox();
            label8 = new Label();
            BuildConfigurationComboBox = new ComboBox();
            SuspendLayout();
            // 
            // button1
            // 
            button1.Location = new Point(55, 82);
            button1.Name = "button1";
            button1.Size = new Size(160, 61);
            button1.TabIndex = 0;
            button1.Text = "開始編譯";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(517, 21);
            label1.Name = "label1";
            label1.Size = new Size(79, 15);
            label1.TabIndex = 1;
            label1.Text = "點擊開始編譯";
            // 
            // HourComboBox
            // 
            HourComboBox.DisplayMember = "0";
            HourComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            HourComboBox.FormattingEnabled = true;
            HourComboBox.Items.AddRange(new object[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" });
            HourComboBox.Location = new Point(12, 175);
            HourComboBox.Name = "HourComboBox";
            HourComboBox.Size = new Size(121, 23);
            HourComboBox.TabIndex = 2;
            HourComboBox.ValueMember = "0";
            HourComboBox.SelectedIndexChanged += HourComboBox_SelectedIndexChanged;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(12, 157);
            label2.Name = "label2";
            label2.Size = new Size(55, 15);
            label2.TabIndex = 3;
            label2.Text = "排定時間";
            // 
            // MinutesComboBox
            // 
            MinutesComboBox.DisplayMember = "0";
            MinutesComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            MinutesComboBox.FormattingEnabled = true;
            MinutesComboBox.Items.AddRange(new object[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" });
            MinutesComboBox.Location = new Point(139, 175);
            MinutesComboBox.Name = "MinutesComboBox";
            MinutesComboBox.Size = new Size(121, 23);
            MinutesComboBox.TabIndex = 4;
            MinutesComboBox.ValueMember = "0";
            MinutesComboBox.SelectedIndexChanged += MinutesComboBox_SelectedIndexChanged;
            // 
            // CountDownText
            // 
            CountDownText.AutoSize = true;
            CountDownText.Location = new Point(12, 211);
            CountDownText.Name = "CountDownText";
            CountDownText.Size = new Size(38, 15);
            CountDownText.TabIndex = 5;
            CountDownText.Text = "00:00";
            // 
            // button2
            // 
            button2.Location = new Point(56, 225);
            button2.Name = "button2";
            button2.Size = new Size(160, 56);
            button2.TabIndex = 6;
            button2.Text = "啟動排程";
            button2.UseVisualStyleBackColor = true;
            button2.Click += button2_Click;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(12, 284);
            label3.Name = "label3";
            label3.Size = new Size(43, 15);
            label3.TabIndex = 7;
            label3.Text = "未排程";
            // 
            // checkedListBox1
            // 
            checkedListBox1.FormattingEnabled = true;
            checkedListBox1.Items.AddRange(new object[] { "正式主機 45.121.51.34", "正式一機 114.32.129.153", "測試機 203.204.228.104", "開發機 203.204.228.101" });
            checkedListBox1.Location = new Point(12, 344);
            checkedListBox1.Name = "checkedListBox1";
            checkedListBox1.Size = new Size(218, 76);
            checkedListBox1.TabIndex = 8;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(12, 326);
            label4.Name = "label4";
            label4.Size = new Size(91, 15);
            label4.TabIndex = 9;
            label4.Text = "需要上傳的機器";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(263, 225);
            label5.Name = "label5";
            label5.Size = new Size(79, 15);
            label5.TabIndex = 10;
            label5.Text = "建置光影地圖";
            // 
            // LightMapBuildCheckList
            // 
            LightMapBuildCheckList.FormattingEnabled = true;
            LightMapBuildCheckList.Location = new Point(263, 257);
            LightMapBuildCheckList.Name = "LightMapBuildCheckList";
            LightMapBuildCheckList.Size = new Size(218, 166);
            LightMapBuildCheckList.TabIndex = 11;
            // 
            // GitBranchComboBox
            // 
            GitBranchComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            GitBranchComboBox.FormattingEnabled = true;
            GitBranchComboBox.Location = new Point(380, 175);
            GitBranchComboBox.Name = "GitBranchComboBox";
            GitBranchComboBox.Size = new Size(121, 23);
            GitBranchComboBox.TabIndex = 12;
            GitBranchComboBox.SelectedIndexChanged += GitBranchComboBox_SelectedIndexChanged;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(380, 157);
            label6.Name = "label6";
            label6.Size = new Size(67, 15);
            label6.TabIndex = 13;
            label6.Text = "編譯的分支";
            // 
            // checkedListBox2
            // 
            checkedListBox2.FormattingEnabled = true;
            checkedListBox2.Items.AddRange(new object[] { "run_build-testing", "run_build-all", "run_build-testing2", "run_build-beta20230905" });
            checkedListBox2.Location = new Point(263, 12);
            checkedListBox2.Name = "checkedListBox2";
            checkedListBox2.Size = new Size(218, 58);
            checkedListBox2.TabIndex = 8;
            checkedListBox2.SelectedIndexChanged += checkedListBox2_SelectedIndexChanged;
            // 
            // checkedListBox3
            // 
            checkedListBox3.FormattingEnabled = true;
            checkedListBox3.Items.AddRange(new object[] { "Git Restore", "Git Pull", "建光+編譯", "只建光" });
            checkedListBox3.Location = new Point(12, 0);
            checkedListBox3.Name = "checkedListBox3";
            checkedListBox3.Size = new Size(218, 58);
            checkedListBox3.TabIndex = 8;
            checkedListBox3.SelectedIndexChanged += checkedListBox3_SelectedIndexChanged;
            // 
            // button3
            // 
            button3.Location = new Point(234, 94);
            button3.Name = "button3";
            button3.Size = new Size(108, 49);
            button3.TabIndex = 14;
            button3.Text = "取消編譯";
            button3.UseVisualStyleBackColor = true;
            button3.Click += button3_Click;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(380, 105);
            label7.Name = "label7";
            label7.Size = new Size(67, 15);
            label7.TabIndex = 16;
            label7.Text = "烘培光品質";
            // 
            // LightQuelity
            // 
            LightQuelity.DropDownStyle = ComboBoxStyle.DropDownList;
            LightQuelity.FormattingEnabled = true;
            LightQuelity.Items.AddRange(new object[] { "Preview", "Medium", "High", "Production", "MAX" });
            LightQuelity.Location = new Point(380, 123);
            LightQuelity.Name = "LightQuelity";
            LightQuelity.Size = new Size(121, 23);
            LightQuelity.TabIndex = 15;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new Point(380, 211);
            label8.Name = "label8";
            label8.Size = new Size(55, 15);
            label8.TabIndex = 18;
            label8.Text = "編譯設定";
            // 
            // BuildConfigurationComboBox
            // 
            BuildConfigurationComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            BuildConfigurationComboBox.FormattingEnabled = true;
            BuildConfigurationComboBox.Items.AddRange(new object[] { "Development", "Release" });
            BuildConfigurationComboBox.Location = new Point(380, 229);
            BuildConfigurationComboBox.Name = "BuildConfigurationComboBox";
            BuildConfigurationComboBox.Size = new Size(121, 23);
            BuildConfigurationComboBox.TabIndex = 17;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(926, 450);
            Controls.Add(label8);
            Controls.Add(BuildConfigurationComboBox);
            Controls.Add(label7);
            Controls.Add(LightQuelity);
            Controls.Add(button3);
            Controls.Add(label6);
            Controls.Add(GitBranchComboBox);
            Controls.Add(LightMapBuildCheckList);
            Controls.Add(label5);
            Controls.Add(label4);
            Controls.Add(checkedListBox3);
            Controls.Add(checkedListBox2);
            Controls.Add(checkedListBox1);
            Controls.Add(label3);
            Controls.Add(button2);
            Controls.Add(CountDownText);
            Controls.Add(MinutesComboBox);
            Controls.Add(label2);
            Controls.Add(HourComboBox);
            Controls.Add(label1);
            Controls.Add(button1);
            Name = "Form1";
            Text = "AutoCompile";
            Load += Form1_Load_2;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button button1;
        private Label label1;
        private ComboBox HourComboBox;
        private Label label2;
        private ComboBox MinutesComboBox;
        private Label CountDownText;
        private Button button2;
        private Label label3;
        private CheckedListBox checkedListBox1;
        private Label label4;
        private Label label5;
        private CheckedListBox LightMapBuildCheckList;
        private ComboBox GitBranchComboBox;
        private Label label6;
        private CheckedListBox checkedListBox2;
        private CheckedListBox checkedListBox3;
        private Button button3;
        private Label label7;
        private ComboBox LightQuelity;
        private Label label8;
        private ComboBox BuildConfigurationComboBox;
    }
}